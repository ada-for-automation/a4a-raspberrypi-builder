# use latest armv7hf compatible raspbian OS version from group balena as base image
FROM balenalib/armv7hf-debian:bookworm

# enable building ARM container on x86 machinery on the web (comment out next line if built on Raspberry)
RUN [ "cross-build-start" ]

# labeling
LABEL maintainer="slos@hilscher.com" \
      version="V0.0.3" \
      description="Debian (bookworm) / Ada for Automation Development"

# version
ENV ADA_FOR_AUTOMATION_BASIS_DEV_VERSION 0.0.3

# install ssh, gcc, create user "pi" and make him sudo
RUN apt-get update  \
    && apt-get install -y openssh-server build-essential \
    && mkdir /var/run/sshd \
    && useradd --create-home --shell /bin/bash pi \
    && echo 'pi:raspberry' | chpasswd \
    && adduser pi sudo
   
# install git, gnat and gprbuild
RUN apt-get update  \
    && apt-get install -y git gnat gprbuild

# install libmodbus
RUN apt-get update  \
    && apt-get install -y libmodbus5 libmodbus-dev

# install Ada for Automation and build some demo applications
RUN mkdir --parents /home/pi/Ada \
    && cd /home/pi/Ada \
    && git clone https://gitlab.com/ada-for-automation/ada-for-automation.git A4A \
    && cd "/home/pi/Ada/A4A/demo/000 a4a-k0-cli" && Arch=armhf make

# SSH port
EXPOSE 22

# the entrypoint shall start ssh
ENTRYPOINT ["/usr/sbin/sshd", "-D"]

# set STOPSIGNAL
STOPSIGNAL SIGTERM

# stop processing ARM emulation (comment out next line if built on Raspberry)
RUN [ "cross-build-end" ]


